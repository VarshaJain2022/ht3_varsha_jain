I have added 2 screen recordings in my repository
A) One recording displays the postman collection execution via the CLI
B) Second recording displays the Jenkins configuration made along with the jenkins job config and the successful execution of postman collection via jenkins

The Github suite consist of 2 json files - one with the Dropbox collection for the Dropbox API and other for the environment variables.
Import both of them into Postman

The Dropbox collection consist of 1 pre-request for fetching oauth token and 3 test cases for the desired functionality  -
Upload file,
File metadata,
Delete file

The Dropbox collection can be executed directly using Postman UI or through the postman CLI using newman.

To install newman
npm install -g newman

To Execute the collection using the created environment variable
newman run DropboxAPI_HT2_Varsha_Jain_Collection.postman_collection.json
-e PostmanTestEnvironment.postman_environment.json

The jenkins build has been created to run the postman collection via Jenkins
Build executed successfully, attached the build log
Build config.xml has also been attached.

In case of any questions, please write back to me in Teams/add a comment in the git repo
